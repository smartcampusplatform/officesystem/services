<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Disposition extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

	protected $table = 'disposition'; //nama table yang kita buat lewat migration 
	
	protected $primaryKey="id_disposition";
	public $timestamps = false;
	/*
	 * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_disposition','id_document','sender','receiver','date','time','note',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Archive;
use Illuminate\Http\Request;


class ArchiveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Archive::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Archive::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201); 
		
		
		
    }

    public function show($id)
    {
        $Archive = Archive::find($id);
        return response()->json($Archive);
		
    }
	
	public function search($keyword)
    {
        //$Archive = Archive::find($id);
		$products = app('db')->select("
            SELECT *
            FROM document, archive
			WHERE document.id_document=archive.id_document AND document.subject LIKE '%".$keyword."%'
        ");
        return response()->json($products);
        
		
    }

    public function update(Request $request, $id)
    {
        $product = Archive::find($id);
        /* $product->update($request->all());
        return response()->json(['message' => 'Successfully update']); */
		
		if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
		
    }

    public function delete($id)
    {
        Archive::destroy($id);
        return response()->json(['message' => 'Successfully delete ']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Archive 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Archive 
			WHERE id_Archive='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Archive 
			WHERE id_Archive='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

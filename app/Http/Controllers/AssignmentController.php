<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Assignment;
use Illuminate\Http\Request;


class AssignmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        //$products = Assignment::all();
		$products = app('db')->select("
            SELECT id_assignment, name, id_employee, date_start, date_end
            FROM assignment
			
        ");
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Assignment::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201);
	
    }

    public function show($id)
    {
        $Assignment = Assignment::find($id);
        return response()->json($Assignment);
    }
	
	public function showprogress()
    {
        $products = app('db')->select("
            SELECT id_assignment, name, id_employee, realisation
            FROM assignment
			
        ");
        return response()->json($products);
    }
	
	public function show_progress($id)
    {
        $products = app('db')->select("
            SELECT id_assignment, name, id_employee, realisation
            FROM assignment
			WHERE id_assignment='".$id."'
        ");
        return response()->json($products);
    }

    public function update(Request $request, $id)
    {
        $product = Assignment::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }
	
	public function approve(Request $request, $id)
    {
        $product = Assignment::find($id);
        if ($product) {
			$products = app('db')->select("
					UPDATE assignment
					SET status=true
					WHERE id_assignment='".$id."'
				");
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }
	
	

    public function delete($id)
    {
        Assignment::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Assignment 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Assignment 
			WHERE id_Assignment='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Assignment 
			WHERE id_Assignment='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

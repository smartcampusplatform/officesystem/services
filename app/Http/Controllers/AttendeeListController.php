<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\AttendeeList;
use Illuminate\Http\Request;


class AttendeeListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = AttendeeList::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        AttendeeList::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201);
		
		
    }

    public function show($id)
    {
        $Meeting = Meeting::find($id);
        return response()->json($Meeting);
    }

    public function update(Request $request, $id)
    {
        $product = Meeting::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Meeting::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Meeting 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Meeting 
			WHERE id_Meeting='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Meeting 
			WHERE id_Meeting='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

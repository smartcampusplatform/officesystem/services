<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Comment;
use Illuminate\Http\Request;


class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Comment::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Comment::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201);
		
		/* print_r($request->input());
		
		$data = new Comment();
		$data->id_comment = $request->input('id_comment');
		$data->id_document = $request->input('id_document');
		$data->id_employee = $request->input('id_employee');
		$data->content = $request->input('content');
		$data->date = $request->input('date');
		$data->time = $request->input('time');
		
		$data->save();			
			
        return response('berhasil'); */
    }

    public function show($id)
    {
        $Comment = Comment::find($id);
        return response()->json($Comment);
    }

    public function update(Request $request, $id)
    {
        $product = Comment::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Comment::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    
	
}

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Disposition;
use Illuminate\Http\Request;


class DispositionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Disposition::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Disposition::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201);
		
		
    }

    public function show($id)
    {
        $Disposition = Disposition::find($id);
        return response()->json($Disposition);
    }

    public function update(Request $request, $id)
    {
        $product = Disposition::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Disposition::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Disposition 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Disposition 
			WHERE id_Disposition='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Disposition 
			WHERE id_Disposition='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

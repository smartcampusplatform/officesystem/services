<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Document;
use Illuminate\Http\Request;


class DocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Document::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Document::create($request->all());
		
		return response()->json(['code'=>'Sukses',],201);
    }

    public function show($id)
    {
        $Document = Document::find($id);
        return response()->json($Document);
    }

    public function update(Request $request, $id)
    {
        $product = Document::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Document::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM document 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM document 
			WHERE id_document='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM document 
			WHERE id_document='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

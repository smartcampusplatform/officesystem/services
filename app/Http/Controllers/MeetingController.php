<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Meeting;
use Illuminate\Http\Request;


class MeetingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Meeting::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Meeting::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201);
		
		/* print_r($request->input());
		
		$data = new Meeting();
		$data->id_meeting = $request->input('id_meeting');
		$data->name = $request->input('name');
		$data->leader = $request->input('leader');
		$data->location = $request->input('location');
		$data->attendee = $request->input('attendee');
		$data->date = $request->input('date');
		$data->time = $request->input('time');
		$data->note = $request->input('note');
		$data->id_notulen = $request->input('id_notulen');
		
		$data->save();			
			
        return response('berhasil'); */
    }

    public function show_schedule($id)
    {
        //$Meeting = Meeting::find($id);
		$query = app('db')->select("
            SELECT id_meeting, date
            FROM Meeting 
			WHERE id_Meeting='".$id."'
        ");
		
        return response()->json($query);
    }

    public function update(Request $request, $id)
    {
        $product = Meeting::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Meeting::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Meeting 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Meeting 
			WHERE id_Meeting='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Meeting 
			WHERE id_Meeting='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

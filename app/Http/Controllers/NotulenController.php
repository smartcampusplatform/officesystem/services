<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Notulen;
use Illuminate\Http\Request;


class NotulenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Notulen::all();
		
        return response()->json($products);
    }

    public function store(Request $request)
    {
       
		Notulen::create($request->all());;

        return response()->json(['code'=>'Sukses',
        ],201); 
		
		
		
		
    }

    public function show($id)
    {
        $Notulen = Notulen::find($id);
        return response()->json($Notulen);
    }

    public function update(Request $request, $id)
    {
        $product = Notulen::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Notulen::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Notulen 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Notulen 
			WHERE id_Notulen='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Notulen 
			WHERE id_Notulen='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB as DB;
use App\Signature;
use Illuminate\Http\Request;


class SignatureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function index()
    {
        $products = Signature::all();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        Signature::create($request->all());

        return response()->json(['code'=>'Sukses',
        ],201);
		
		
		
    }

    public function show($id)
    {
        $Signature = Signature::find($id);
        return response()->json($Signature);
    }

    public function update(Request $request, $id)
    {
        $product = Signature::find($id);
        if ($product) {
			$product->update($request->all());
			return response()->json([
				'message' => 'Successfully update'
			]);
		}

		return response()->json([
			'message' => 'id not found',
				], 404);
    }

    public function delete($id)
    {
        Signature::destroy($id);
        return response()->json(['message' => 'Successfully delete']);
    }



    //
	/* public function index(){

        $query = app('db')->select("
            SELECT *
            FROM Signature 
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function show($id){

        $query = app('db')->select("
            SELECT *
            FROM Signature 
			WHERE id_Signature='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    }
	
	public function store(){

        $query = app('db')->select("
            INSERT *
            FROM Signature 
			WHERE id_Signature='".$id."'
        ");
        
		   return json_encode($query, JSON_PRETTY_PRINT);

    } */
	
}

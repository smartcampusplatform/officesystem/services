PGDMP     '    $                w            smartoffice_db    10.7    10.7 #    (           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            )           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            *           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            +           1262    16393    smartoffice_db    DATABASE     �   CREATE DATABASE smartoffice_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE smartoffice_db;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            ,           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            -           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16394    archive    TABLE     �   CREATE TABLE public.archive (
    id_archive character(25) NOT NULL,
    id_document character(25) NOT NULL,
    archiver character(25),
    date date NOT NULL,
    "time" abstime,
    status boolean NOT NULL
);
    DROP TABLE public.archive;
       public         postgres    false    3            �            1259    16397 
   assignment    TABLE     *  CREATE TABLE public.assignment (
    id_assignment character(25) NOT NULL,
    name_assignment character(25) NOT NULL,
    "dateStart" date NOT NULL,
    "dateEnd" date NOT NULL,
    duration integer,
    status integer,
    "real" integer,
    file path,
    id_employee character(25) NOT NULL
);
    DROP TABLE public.assignment;
       public         postgres    false    3            �            1259    16403    comment    TABLE     �   CREATE TABLE public.comment (
    id_comment character(25) NOT NULL,
    id_document character(25) NOT NULL,
    id_employee character(25) NOT NULL,
    content text NOT NULL,
    date date NOT NULL,
    "time" abstime NOT NULL
);
    DROP TABLE public.comment;
       public         postgres    false    3            �            1259    16409    disposition    TABLE       CREATE TABLE public.disposition (
    id_disposition character(25) NOT NULL,
    id_document character(25) NOT NULL,
    sender_disposition character(25) NOT NULL,
    receiver_disposition character(25) NOT NULL,
    date date NOT NULL,
    "time" abstime,
    note character(25)
);
    DROP TABLE public.disposition;
       public         postgres    false    3            �            1259    16412    document    TABLE     <  CREATE TABLE public.document (
    id_document character(25) NOT NULL,
    number character(25) NOT NULL,
    date date NOT NULL,
    sender character(25) NOT NULL,
    receiver character(25) NOT NULL,
    type character(25) NOT NULL,
    content text,
    id_signature character(25),
    status boolean NOT NULL
);
    DROP TABLE public.document;
       public         postgres    false    3            �            1259    16418    employee    TABLE     �   CREATE TABLE public.employee (
    id_employee character(25) NOT NULL,
    name character(25) NOT NULL,
    "position" character(25) NOT NULL,
    section character(25) NOT NULL,
    division character(25) NOT NULL,
    phone character(25)
);
    DROP TABLE public.employee;
       public         postgres    false    3            �            1259    16421    meeting    TABLE     :  CREATE TABLE public.meeting (
    id_meeting character(25) NOT NULL,
    name character(25) NOT NULL,
    leader character(25) NOT NULL,
    location character(25) NOT NULL,
    attendee integer NOT NULL,
    date date NOT NULL,
    "time" abstime,
    note text NOT NULL,
    id_notulen character(25) NOT NULL
);
    DROP TABLE public.meeting;
       public         postgres    false    3            �            1259    16432    notulen    TABLE     s   CREATE TABLE public.notulen (
    id_notulen character(25) NOT NULL,
    content text,
    notulis character(1)
);
    DROP TABLE public.notulen;
       public         postgres    false    3            �            1259    16438 	   signature    TABLE     �   CREATE TABLE public.signature (
    id_signature character(25) NOT NULL,
    id_employee character(25) NOT NULL,
    file_signature path
);
    DROP TABLE public.signature;
       public         postgres    false    3                      0    16394    archive 
   TABLE DATA               Z   COPY public.archive (id_archive, id_document, archiver, date, "time", status) FROM stdin;
    public       postgres    false    196   �'                 0    16397 
   assignment 
   TABLE DATA               �   COPY public.assignment (id_assignment, name_assignment, "dateStart", "dateEnd", duration, status, "real", file, id_employee) FROM stdin;
    public       postgres    false    197   �'                 0    16403    comment 
   TABLE DATA               ^   COPY public.comment (id_comment, id_document, id_employee, content, date, "time") FROM stdin;
    public       postgres    false    198   �'                  0    16409    disposition 
   TABLE DATA               �   COPY public.disposition (id_disposition, id_document, sender_disposition, receiver_disposition, date, "time", note) FROM stdin;
    public       postgres    false    199   (       !          0    16412    document 
   TABLE DATA               t   COPY public.document (id_document, number, date, sender, receiver, type, content, id_signature, status) FROM stdin;
    public       postgres    false    200   �(       "          0    16418    employee 
   TABLE DATA               [   COPY public.employee (id_employee, name, "position", section, division, phone) FROM stdin;
    public       postgres    false    201   �*       #          0    16421    meeting 
   TABLE DATA               o   COPY public.meeting (id_meeting, name, leader, location, attendee, date, "time", note, id_notulen) FROM stdin;
    public       postgres    false    202   �,       $          0    16432    notulen 
   TABLE DATA               ?   COPY public.notulen (id_notulen, content, notulis) FROM stdin;
    public       postgres    false    203   4.       %          0    16438 	   signature 
   TABLE DATA               N   COPY public.signature (id_signature, id_employee, file_signature) FROM stdin;
    public       postgres    false    204   y/       �
           2606    16446    archive Archive_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.archive
    ADD CONSTRAINT "Archive_pkey" PRIMARY KEY (id_archive);
 @   ALTER TABLE ONLY public.archive DROP CONSTRAINT "Archive_pkey";
       public         postgres    false    196            �
           2606    16448    assignment Assignment_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT "Assignment_pkey" PRIMARY KEY (id_assignment);
 F   ALTER TABLE ONLY public.assignment DROP CONSTRAINT "Assignment_pkey";
       public         postgres    false    197            �
           2606    16450    comment Comment_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.comment
    ADD CONSTRAINT "Comment_pkey" PRIMARY KEY (id_comment);
 @   ALTER TABLE ONLY public.comment DROP CONSTRAINT "Comment_pkey";
       public         postgres    false    198            �
           2606    16452    disposition Disposition_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.disposition
    ADD CONSTRAINT "Disposition_pkey" PRIMARY KEY (id_disposition);
 H   ALTER TABLE ONLY public.disposition DROP CONSTRAINT "Disposition_pkey";
       public         postgres    false    199            �
           2606    16454    employee Employee_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.employee
    ADD CONSTRAINT "Employee_pkey" PRIMARY KEY (id_employee);
 B   ALTER TABLE ONLY public.employee DROP CONSTRAINT "Employee_pkey";
       public         postgres    false    201            �
           2606    16456    meeting Meeting_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.meeting
    ADD CONSTRAINT "Meeting_pkey" PRIMARY KEY (id_meeting);
 @   ALTER TABLE ONLY public.meeting DROP CONSTRAINT "Meeting_pkey";
       public         postgres    false    202            �
           2606    16458    signature Signature_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.signature
    ADD CONSTRAINT "Signature_pkey" PRIMARY KEY (id_signature);
 D   ALTER TABLE ONLY public.signature DROP CONSTRAINT "Signature_pkey";
       public         postgres    false    204            �
           2606    16460    document document_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id_document);
 @   ALTER TABLE ONLY public.document DROP CONSTRAINT document_pkey;
       public         postgres    false    200            �
           2606    16464    notulen notulen_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.notulen
    ADD CONSTRAINT notulen_pkey PRIMARY KEY (id_notulen);
 >   ALTER TABLE ONLY public.notulen DROP CONSTRAINT notulen_pkey;
       public         postgres    false    203               N   x�sr60T�8]���������c�120��50�50���,�r�e�tc�2��y%�Xe ���AL����� �J            x������ � �            x������ � �          �   x�s�00V��.���X$8�s��ұ�8'f%b�cd`h�k`�kh��D\. �qYl�������bs�bS�ŎEřىy� ��f-�=&XmuI-.��*㟛X�M= �K�KB|p���`�`�1N�Č�L�2��y%�eP݃;|b���� _6_      !   �  x��KO�0��ɧ� Q�n%z,l_j��^{v�/l�߾cB�ne�w؜�y�??9�|��Wй����s6�ޝ���y5\�όݠ��빒�.�?�÷���"lt1C�
詞�����T%ZN\�p�9Vo!�@,�G�|�l�-[	����T��w�	Np]�{-��ͽ>�?�;�!���=P6\�r Ρ7q�܂t���$'B���Lq�@[�	6�$J%�,:ɲ,Ȕ2�R��us5�G'Iۑ�����J`ع?�B[ٱj�@�0��	><J���Dc���Il���"�ȖBS�)�M���mo�ۭʌ`�Pn^]�@�r�']�O�՗O��+��F9;�������`��<�g_)I�����K�f-���Z�9;?��G��]�g��b���ّ��ٺ��Z�8[��gգt�n�J��p����Z�9[8�.���u��uq����i�ߌ��B      "   �  x���KO�@���W̪Kc��T���7���N�f���CM���hd5�	9s�90B����sB����)"�2����-wVKCiXe���@:kwiƱ�!+g��w�����aI�3,���`�i���(�GciU2��S)��k'�����"�Sm��V����P*E�/�׋7���!�3z�e��]�;�B��V���ͅ�v��-ؤm��r�Dd%x�G��BO]8o��}�%�KMVj�}~�BO]8o��J*�m�4��*]q�����h�B���NԌc�E���Z�6�m��IF[��k�⑵2-��B-���(�����5�Jl���&f-�yЧ���[��d���.xЧw1P��x�xЧ��g8{dkЧ��C>ޒ���}��j�[��A��9F�[�>�G:��!����`��m����o����6̐cZ�9�y"%#[`�C/������[�w��y���| y��      #   �  x��R�N1<�|E} �vW���=H�ȍK��]Z�ۍ���3���E���v����u���n�x�M7T�㆓�D��P�4*��g�-֣��X;���6�o?�o>�o�����%W���4��+�tP�~�Sc׹ky)�ĉs�6��m1{t�ź%9��5uhG���+6#��TP���;8I4pD�G�R<ÃJCʭW��'�N:u�	E�W�q�H�I�!��e0�h���F���3��1Y�JB�R����j���1-6���)��p�_�L���%�!��Ѻ`��t��y��#vv�io/g���,��pq8eC��BC7��`6<7��s4h$fG���e��z�?���j$j�r��qmb��/Sӧ��T�^����=Qo#Q�r3�-�<�k��      $   5  x��MN1�םS�T#�t��vlL�K�kbW=>+n���$����||~9�����{��3�ˈ5Վ!
ʬ{�Ze���dH�r'����;�b#��ܼ[J�(ъ���]��6#ӹ(��hū��dG��s�#�=.&���7�A�Tj��D9�My��9�KR��ɝg�T�>JW<NI2eH7w����έ���ݓ�ŵ&k>�ݎ'����"d8�YHQ�!4�~���pܔmrt5��u��D���)Z��L�����ܨ��c&D��k�iڠ	H���jy���\���j��j��eY>�zT]      %   �   x�}�9�0k�yA`J��0ܤQJ��Iav;�V��ݹok\�Y�󻇺Z�O��3��
�u�[2�~�1�)���z��'b���b��+cf7��)Ʌq��(	�{��q�1w:�j�s&�%�LrKҙ䖤1ٹ%��N��N����ݴ�P     
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/key', function(){
    $key = str_random(32);
    return $key;
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
	
	$router->get('/archive', 'ArchiveController@index');
	$router->get('/archive/{id}', 'ArchiveController@show');
	$router->get('/searcharchive/{keyword}', 'ArchiveController@search');
	$router->post('/createarchive', 'ArchiveController@store');
	$router->delete('/archive/{id}', 'ArchiveController@delete');
	$router->put('/archive/{id}', 'ArchiveController@update');
	
	$router->get('/assignment', 'AssignmentController@index');
	$router->get('/assignment/{id}', 'AssignmentController@show');
	$router->post('/assignment', 'AssignmentController@store');
	$router->delete('/assignment/{id}', 'AssignmentController@delete');
	$router->put('/assignment/{id}', 'AssignmentController@update');
	$router->put('/setprogress/{id}', 'AssignmentController@update');
	$router->get('/getprogress', 'AssignmentController@showprogress');
	$router->get('/getprogress/{id}', 'AssignmentController@show_progress');
	$router->put('/approveassignment/{id}', 'AssignmentController@approve');
	$router->put('/attachment/{id}', 'AssignmentController@update');
	
	$router->get('/comment', 'CommentController@index');
	$router->get('/comment/{id}', 'CommentController@show');
	$router->post('/comment', 'CommentController@store');
	$router->delete('/comment/{id}', 'CommentController@delete');
	$router->put('/comment/{id}', 'CommentController@update');
	
	$router->get('/disposition', 'DispositionController@index');
	$router->get('/disposition/{id}', 'DispositionController@show');
	$router->post('/createdisposition', 'DispositionController@store');
	$router->delete('/disposition/{id}', 'DispositionController@delete');
	$router->put('/disposition/{id}', 'DispositionController@update');
    
	$router->get('/document', 'DocumentController@index');
	$router->get('/document/{id}', 'DocumentController@show');
	$router->post('/createdocument', 'DocumentController@store');
	$router->get('/reviewdocument', 'DocumentController@review_index');
	$router->get('/reviewdocument/{id}', 'DocumentController@review_show');
	$router->delete('/document/{id}', 'DocumentController@delete');
	$router->put('/revisedocument/{id}', 'DocumentController@update');
	$router->put('/approvedocument/{id}', 'DocumentController@approve');
	$router->post('/adddestination', 'DocumentController@store_destination');
	$router->get('/destination', 'DocumentController@index_destination');
	
	$router->get('/meeting', 'MeetingController@index');
	$router->get('/meeting/{id}', 'MeetingController@show');
	$router->post('/meeting', 'MeetingController@store');
	$router->delete('/meeting/{id}', 'MeetingController@delete');
	$router->put('/meeting/{id}', 'MeetingController@update');
	$router->get('/getschedule/{id}', 'MeetingController@show_schedule');
    
	$router->get('/notulen', 'NotulenController@index');
	$router->get('/notulen/{id_notulen}', 'NotulenController@show');
	$router->post('/notulen', 'NotulenController@store');
	$router->delete('/notulen/{id}', 'NotulenController@delete');
	$router->put('/notulen/{id_notulen}', 'NotulenController@update');
	
	$router->get('/signature', 'SignatureController@index');
	$router->get('/signature/{id}', 'SignatureController@show');
	$router->post('/signature', 'SignatureController@store');
	$router->delete('/signature{id}', 'SignatureController@delete');
	$router->put('/signature/{id}', 'SignatureController@update');
	
	$router->post('/addattendee', 'AttendeeListController@store');
	$router->get('/attendee', 'AttendeeListController@index');
	$router->get('/attendee/{id}', 'AttendeeListController@show');
	
    

});


